import React from 'react';
import Aux from '../../Hoc/AuxContainer';
import './Layout.css';

/**
 * Layout Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Layout component
 * 
 * @param {object} props : object with the props received
 */
const Layout = (props) => (
    <Aux>
        <div>Toolbar, SideDrawer, Backdrop </div>
        <main className="layout">
            {props.children}
        </main>
    </Aux>
);

export default Layout;
