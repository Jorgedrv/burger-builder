import React from 'react';
import './BuildControls.css';
import BuildControl from './Build-control/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }, 
];

/**
 * Builder Controls Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Build controls for wrap the ingredients
 * 
 * @param {object} props : props recieved 
 */
const BuildControls = (props) => (
    <div className="build-controls">
        <p>Current Price: <strong>$ {props.price.toFixed(2)}</strong></p>
        {controls.map(ctrl => (
            <BuildControl 
                key={ctrl.label} 
                label={ctrl.label} 
                ingredientAdded={() => props.ingredientAdded(ctrl.type)}
                ingredientRemoved={() => props.ingredientRemoved(ctrl.type)}
                disabledRemove={props.disabledRemove[ctrl.type]}
                disabledAdd={props.disabledAdd[ctrl.type]}
            />
        ))}
    </div>
);

export default BuildControls;
