import React from 'react';
import './BuildControl.css';

/**
 * Builder Control Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Build control component for add ingredients
 * 
 * @param {object} props : props recieved
 */
const BuildControl = (props) => (
    <div className="build-control">
        <div className="label">{props.label}</div>
        <button 
            className="more" 
            onClick={props.ingredientAdded}
            disabled={props.disabledAdd}>Add</button>
        <button 
            className="less" 
            onClick={props.ingredientRemoved}
            disabled={props.disabledRemove}>Remove</button>
    </div>
);

export default BuildControl;
