import React from 'react';
import './Burger.css';
import Ingredient from './Ingredients/Ingredient';

/**
 * Burger Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Burger component to put the size of the burger and add the ingredients
 * 
 */
const Burger = (props) => {
    let transformedIngredients = Object.keys(props.ingredients).map(ingKey => {
        return [...Array(props.ingredients[ingKey])].map((_, i) => {
            return <Ingredient key={ingKey + i} type={ingKey} />
        });
    }).reduce((arr, el) => { return arr.concat(el)}, [] );
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients!</p>;
    }
    return (
        <div className="burger">
            <Ingredient type="bread-top" />
                {transformedIngredients}
            <Ingredient type="bread-bottom" />
        </div>
    );
};

export default Burger;
