import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Ingredient.css';

/**
 * Ingredient Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Ingredient component for burger
 * 
 * @param {object} props : props received
 */
class Ingredient extends Component {

    /**
     * Default constructor
     */
    constructor(props) {
        super(props);
        this.state = {
            breadBottom: 'bread-bottom',
            breadTop: 'bread-top',
            meat: 'meat',
            cheese: 'cheese',
            bacon: 'bacon',
            salad: 'salad',
        }
    }

    /**
     * Default JSX Wrapper
     */
    render() {
        const { breadBottom, breadTop, meat, cheese, bacon, salad } = this.state;
        let ingredient = null;

        switch (this.props.type) {
            case (breadBottom):
                ingredient = <div className={breadBottom}></div>
                break;
            case (breadTop):
                ingredient = (
                    <div className={breadTop}>
                        <div className="seeds1"></div>
                        <div className="seeds2"></div>
                    </div>
                );
                break;
            case (meat):
                ingredient = <div className={meat}></div>
                break;
            case (cheese):
                ingredient = <div className={cheese}></div>
                break;
            case (bacon):
                ingredient = <div className={bacon}></div>
                break;
            case (salad):
                ingredient = <div className={salad}></div>
                break;
            default:
                ingredient = null;
        }

        return ingredient;
    }
}

Ingredient.propTypes = {
    type: PropTypes.string.isRequired,
};

export default Ingredient;