import React, { Component } from 'react';
import Aux from '../../Hoc/AuxContainer';
import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Build-controls/BuildControls';

const INGREDIENT_PRICE = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7,
};

/**
 * Builder Burger Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */
class BurgerBuilder extends Component {
    
    /**
     * Default constructor
     */
    constructor() {
        super();
        this.state = {
            ingredients: {
                salad: 0,
                bacon: 0,
                cheese: 0,
                meat: 0,
            },
            totalPrice: 4,
        }
    }

    /**
     * Allows to add ingredients to a burger
     * 
     * @param {string} type : name of the ingridient to add 
     */
    addIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = oldCount + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIENT_PRICE[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;
        this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
    }
    
    /**
     * Allows to remove ingredients of the burger
     * 
     * @param {string} type : name of the ingridient to remove 
     */
    removedIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type];
        const updatedCount = (oldCount <= 0) ? 0 : oldCount - 1;
        const updatedIngredients = {
            ...this.state.ingredients
        };
        updatedIngredients[type] = updatedCount;
        const priceDeduction = INGREDIENT_PRICE[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceDeduction;
        this.setState({ ingredients: updatedIngredients, totalPrice: newPrice });
    }

    /**
     * Allows to disable the remove button if the ingredient is equals to 0
     */
    disabledRemove() {
        const disabled = {
            ...this.state.ingredients
        };
        for (let key in disabled) {
            disabled[key] = disabled[key] <= 0
        }
        return disabled;
    }

    /**
     * Allows to disable the add button if the ingredient is major than 2
     */
    disabledAdd() {
        const disabled = {
            ...this.state.ingredients
        };
        for (let key in disabled) {
            disabled[key] = disabled[key] > 1
        }
        return disabled;
    }

    /**
     * Default JSX Wrapper
     */
    render() {
        return (
            <Aux>
                <Burger ingredients={this.state.ingredients} />
                <BuildControls 
                    ingredientAdded={this.addIngredientHandler} 
                    ingredientRemoved={this.removedIngredientHandler} 
                    disabledRemove={this.disabledRemove()}
                    disabledAdd={this.disabledAdd()}
                    price={this.state.totalPrice}
                />  
            </Aux>
        );
    }
}

export default BurgerBuilder;
