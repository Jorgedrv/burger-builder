/**
 * Higher Order Component
 *
 * @version        1.0.0 - 03 Nov 2018
 * @author         Jorge Romero - Jorgeddrv@gmail.com
 * @since          0.1.0 - 03 Jun 2018
 *
 */

/**
 * Higher Order Component to shows any JSX inside of render
 * 
 * @param {JSX} props : contains the children to shows up 
 */
const AuxContainer = (props) => props.children;

export default AuxContainer;
